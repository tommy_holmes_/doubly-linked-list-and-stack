// Part 1

public class Node<T> {
    
    var value: T
    
    init(value: T) {
        self.value = value
    }
    
    var next: Node?
    weak var previous: Node?
}

public class LinkedList<T> {
    fileprivate var head: Node<T>?
    private var tail: Node<T>?
    
    public var isEmpty: Bool {
        return head == nil
    }
    
    public var first: Node<T>? {
        return head
    }
    
    public var last: Node<T>? {
        return tail
    }
    
    public var length: Int? {
        var count = 0
        var node = head
        
        if isEmpty { return nil }
        
        while head != nil {
            if node != nil {
                count += 1
                node = node!.next
            }
        }
        return count
    }
    
    public func add(_ value: T) {
        let newNode = Node(value: value)
        
        if let tailNode = tail {
            newNode.previous = tailNode
            tailNode.next = newNode
        } else {
            head = newNode
        }
        
        tail = newNode
    }
    
    public func nodeAt(index: Int) -> Node<T>? {
        if index >= 0 {
            var node = head
            var i = index
            
            while node != nil {
                if i == 0 { return node }
                i -= 1
                node = node!.next
            }
        }
        return nil
    }
    
    public func remove(node: Node<T>) -> T {
        let prev = node.previous
        let next = node.next
        
        if let prev = prev {
            prev.next = next
        } else {
            head = next
        }
        next?.previous = prev
        
        if next == nil {
            tail = prev
        }
        
        node.previous = nil
        node.next = nil
        
        return node.value
    }
    
}

extension LinkedList: CustomStringConvertible {
    public var description: String {
        var text = "H-"
        var node = head
        while node != nil {
            text += "[\(node!.value)]"
            node = node!.next
            if node != nil { text += "-" }
        }
        return text + "-T"
    }
}

let fibbonacci = LinkedList<String>()
fibbonacci.add("zero")
fibbonacci.add("one")
fibbonacci.add("one")
fibbonacci.add("two")
fibbonacci.add("three")
fibbonacci.add("five")

print(fibbonacci)

fibbonacci.nodeAt(index: 4)?.value
print(fibbonacci.remove(node: Node.init(value: "two")))

// Part 2

public struct Stack {
    fileprivate var array: [String] = []
    
    
    mutating func push(_ element: String) {
        array.append(element)
    }
    
    mutating func pop() -> String? {
        return array.popLast()
    }
    
    var peek: String? { return array.last }
    var size: Int? { return array.count }
    
}

var bookStack = Stack()

bookStack.push("Some book")
bookStack.push("Some other book")
bookStack.peek
bookStack.size
bookStack.pop()
bookStack.pop()
bookStack.pop()
bookStack.size

extension Stack: CustomStringConvertible {
    public var description: String {
        var orderedArray: [String] = []
        
        orderedArray = array.reversed()
        
        var text = "["
        
        for (item, _) in orderedArray.enumerated() {
            if item < orderedArray.count - 1 {
                text += "\(orderedArray[item]), "
            } else {
                text += "\(orderedArray[item])"
            }
        }
        text += "]"
        
        return text
    }
    
}

bookStack.push("Some book")
bookStack.push("Some other book")
bookStack.push("Yet another book")
print(bookStack.description)
